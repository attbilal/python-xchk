import subprocess

with open("hosts_and_ports.txt") as hp_fh:
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        ip_adres = hp_pair.split(":")[0]
        poort = hp_pair.split(":")[1]               
        with open("commands.txt") as fh:
            completed = subprocess.run("ssh ubuntussh@+(ip_adres) -p (poort)", capture_output=True, text=True, shell=True, stdin=fh)

